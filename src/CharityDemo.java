public class CharityDemo
{//Start of class
   public static void main(String[] args)
   {//Start of main

      //Instantiate the Charity objects.
      Charity redCross = new Charity();
      Charity heartFund = new Charity();
      Charity unitedFund = new Charity();
   	
      //Test the redCross object.
      redCross.makeContribution(10);
      redCross.makeContribution(20);
      redCross.makeContribution(15);
      System.out.printf("Red Cross total is $%.2f.\n", redCross.getTotal() );
      System.out.printf("Red Cross count is %d.\n", redCross.getCount() );
      System.out.printf("Red Cross smallest is $%.2f.\n", redCross.getSmallest() );
      System.out.printf("Red Cross largest is $%.2f.\n", redCross.getLargest() );
      System.out.printf("Red Cross average is $%.2f.\n", redCross.getAverage() );
      System.out.println();
         	
      //Test the unitedFund object.
      unitedFund.makeContribution(12);
      unitedFund.makeContribution(22);
      unitedFund.makeContribution(17);
      unitedFund.makeContribution(17);
      System.out.printf("United Fund total is $%.2f.\n", unitedFund.getTotal() );
      System.out.printf("United Fund count is %d.\n", unitedFund.getCount() );
      System.out.printf("United Fund smallest is $%.2f.\n", unitedFund.getSmallest() );
      System.out.printf("United Fund largest is $%.2f.\n", unitedFund.getLargest() );
      System.out.printf("United Fund average is $%.2f.\n", unitedFund.getAverage() );
      System.out.println();

      //Test the heartFund object.
      heartFund.makeContribution(14);
      heartFund.makeContribution(24);
      heartFund.makeContribution(19);
      heartFund.makeContribution(19);
      heartFund.makeContribution(19);
      System.out.printf("Heart Fund total is $%.2f.\n", heartFund.getTotal() );
      System.out.printf("Heart Fund count is %d.\n", heartFund.getCount() );
      System.out.printf("Heart Fund smallest is $%.2f.\n", heartFund.getSmallest() );
      System.out.printf("Heart Fund largest is $%.2f.\n", heartFund.getLargest() );
      System.out.printf("Heart Fund average is $%.2f.\n", heartFund.getAverage() );
      System.out.println();
      
   }//End of main
}//End of class