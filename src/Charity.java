import java.util.*;

public class Charity{
	
	int count;
	double var, total, smallest, largest, avg;
	ArrayList<Double> my_array = new ArrayList<Double>();

	public Charity() {
		count = -1;
		total = 0;
		avg = 0;
		smallest = 0;
		largest = 0;
	}
	
	public void makeContribution(double c) {
		count = getCount();
		total = getTotal() + c;
		avg = getAverage();
		my_array.add(c);
	}
	
	public int getCount() {
		count++;
		return count;
	}
	
	public double getSmallest() {
		int minIndex;
		minIndex = my_array.indexOf(Collections.min(my_array));
		smallest = my_array.get(minIndex);
		return smallest;
	}
	
	 public double getLargest() {
		 int maxIndex;
		 maxIndex = my_array.indexOf(Collections.max(my_array));
		 largest = my_array.get(maxIndex);
		 return largest;
	 }
	 
	 public double getTotal() {
		 return total;
	 }
	 
	 public double getAverage() {
		 avg = total / count;
		 return avg;
	 }
}